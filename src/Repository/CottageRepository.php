<?php

namespace App\Repository;

use App\Entity\Cottage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Cottage|null find($id, $lockMode = null, $lockVersion = null)
 * @method Cottage|null findOneBy(array $criteria, array $orderBy = null)
 * @method Cottage[]    findAll()
 * @method Cottage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CottageRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Cottage::class);
    }

    public function findBySearch(array $data)
    {
        $result = $this->createQueryBuilder('b')
            ->select('b')
        ;

        if (!empty(trim($data['name']))) {
            $result
                ->andWhere('b.name LIKE :name')
                ->setParameter("name", $data['name'])
            ;
        }

        if ($data['priceFrom'] > 0) {
            $result
                ->andWhere('b.pricePerDay >= :priceFrom')
                ->setParameter("priceFrom", $data['priceFrom'])
            ;
        }

        if ($data['priceTo'] > 0) {
            $result
                ->andWhere('b.pricePerDay >= :priceTo')
                ->setParameter("priceTo", $data['priceTo'])
            ;
        }

        if ($data['dopField']['Kitchen'] == 'true') {
            $result
                ->andWhere('b.Kitchen = :Kitchen')
                ->setParameter("Kitchen", 1)
            ;
        }

        if ($data['dopField']['bathroom'] == 'true') {
            $result
                ->andWhere('b.bathroom = :bathroom')
                ->setParameter("bathroom", 1)
            ;
        }

        if ($data['dopField']['jacuzzi'] == 'true') {
            $result
                ->andWhere('b.jacuzzi = :jacuzzi')
                ->setParameter("jacuzzi", 1)
            ;
        }

        if ($data['dopField']['pool'] == 'true') {
            $result
                ->andWhere('b.pool = :pool')
                ->setParameter("pool", 1)
            ;
        }

        if ($data['dopField']['garage'] == 'true') {
            $result
                ->andWhere('b.garage = :garage')
                ->setParameter("garage", 1)
            ;
        }

        return $result
            ->orderBy("b.id", 'desc')
            ->getQuery()
            ->getResult();
    }
}
