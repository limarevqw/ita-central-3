<?php

namespace App\Repository;

use App\Entity\Pension;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Pension|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pension|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pension[]    findAll()
 * @method Pension[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PensionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Pension::class);
    }

    public function findBySearch(array $data)
    {
        $result = $this->createQueryBuilder('b')
            ->select('b')
            ;

        if (!empty(trim($data['name']))) {
            $result
                ->andWhere('b.name LIKE :name')
                ->setParameter("name", $data['name'])
            ;
        }

        if ($data['priceFrom'] > 0) {
            $result
                ->andWhere('b.pricePerDay >= :priceFrom')
                ->setParameter("priceFrom", $data['priceFrom'])
                ;
        }

        if ($data['priceTo'] > 0) {
            $result
                ->andWhere('b.pricePerDay >= :priceTo')
                ->setParameter("priceTo", $data['priceTo'])
            ;
        }

        if ($data['dopField']['shower'] == 'true') {
            $result
                ->andWhere('b.shower = :shower')
                ->setParameter("shower", 1)
            ;
        }

        if ($data['dopField']['bunkBeds'] == 'true') {
            $result
                ->andWhere('b.bunkBeds = :bunkBeds')
                ->setParameter("bunkBeds", 1)
            ;
        }

        if ($data['dopField']['BigTV'] == 'true') {
            $result
                ->andWhere('b.BigTV = :BigTV')
                ->setParameter("BigTV", 1)
            ;
        }

        return $result
            ->orderBy("b.id", 'desc')
            ->getQuery()
            ->getResult();
    }

}
