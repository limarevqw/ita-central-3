<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PensionRepository")
 */
class Pension extends BookingObject
{
    /**
     * @ORM\Column(type="boolean")
     */
    private $shower;

    /**
     * @ORM\Column(type="boolean")
     */
    private $bunkBeds;

    /**
     * @ORM\Column(type="boolean")
     */
    private $BigTV;

    /**
     * @param mixed $shower
     * @return Pension
     */
    public function setShower($shower)
    {
        $this->shower = $shower;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getShower()
    {
        return $this->shower;
    }

    /**
     * @param mixed $bunkBeds
     * @return Pension
     */
    public function setBunkBeds($bunkBeds)
    {
        $this->bunkBeds = $bunkBeds;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBunkBeds()
    {
        return $this->bunkBeds;
    }

    /**
     * @param mixed $BigTV
     * @return Pension
     */
    public function setBigTV($BigTV)
    {
        $this->BigTV = $BigTV;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBigTV()
    {
        return $this->BigTV;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $arrayBookingObject = $this->getToArray();
        $array = [
            'shower' => $this->getShower(),
            'bunkBeds' => $this->getShower(),
            'BigTV' => $this->getShower()
        ];

        return array_merge($arrayBookingObject, $array);
    }
}
