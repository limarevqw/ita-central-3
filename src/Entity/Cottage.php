<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CottageRepository")
 */
class Cottage extends BookingObject
{
    /**
     * @ORM\Column(type="boolean")
     */
    private $Kitchen;

    /**
     * @ORM\Column(type="boolean")
     */
    private $bathroom;

    /**
     * @ORM\Column(type="boolean")
     */
    private $jacuzzi;

    /**
     * @ORM\Column(type="boolean")
     */
    private $pool;

    /**
     * @ORM\Column(type="boolean")
     */
    private $garage;

    /**
     * @param mixed $Kitchen
     * @return Cottage
     */
    public function setKitchen($Kitchen)
    {
        $this->Kitchen = $Kitchen;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getKitchen()
    {
        return $this->Kitchen;
    }

    /**
     * @param mixed $bathroom
     * @return Cottage
     */
    public function setBathroom($bathroom)
    {
        $this->bathroom = $bathroom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBathroom()
    {
        return $this->bathroom;
    }

    /**
     * @param mixed $jacuzzi
     * @return Cottage
     */
    public function setJacuzzi($jacuzzi)
    {
        $this->jacuzzi = $jacuzzi;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getJacuzzi()
    {
        return $this->jacuzzi;
    }

    /**
     * @param mixed $pool
     * @return Cottage
     */
    public function setPool($pool)
    {
        $this->pool = $pool;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPool()
    {
        return $this->pool;
    }

    /**
     * @param mixed $garage
     * @return Cottage
     */
    public function setGarage($garage)
    {
        $this->garage = $garage;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGarage()
    {
        return $this->garage;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        $arrayBookingObject = $this->getToArray();
        $array = [
            'Kitchen' => $this->getKitchen(),
            'bathroom' => $this->getBathroom(),
            'jacuzzi' => $this->getJacuzzi(),
            'pool' => $this->getPool(),
            'garage' => $this->getGarage()
        ];

        return array_merge($arrayBookingObject, $array);
    }
}