<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\DiscriminatorColumn;
use Doctrine\ORM\Mapping\DiscriminatorMap;
use Doctrine\ORM\Mapping\InheritanceType;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BookingObjectRepository")
 * @InheritanceType("JOINED")
 * @DiscriminatorColumn(name="discriminator", type="string")
 * @DiscriminatorMap({"bookingObject" = "BookingObject", "pension" = "Pension", "cottage" = "Cottage"})
 */
class BookingObject
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="integer", length=11)
     */
    private $numberRooms;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $contactPerson;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="decimal")
     */
    private $pricePerDay;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $latCoord;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $longCoord;

    /**
     * @var Landlord
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Landlord", inversedBy="bookingObjects")
     * @ORM\JoinColumn(name="landlord_id", referencedColumnName="id")
     */
    private $author;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="bookingObject")
     */
    private $reservations;

    /**
     * @param int $id
     * @return BookingObject
     */
    public function setId(int $id): BookingObject
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $name
     * @return BookingObject
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $numberRooms
     * @return BookingObject
     */
    public function setNumberRooms($numberRooms)
    {
        $this->numberRooms = $numberRooms;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNumberRooms()
    {
        return $this->numberRooms;
    }

    /**
     * @param mixed $contactPerson
     * @return BookingObject
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param mixed $phone
     * @return BookingObject
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $address
     * @return BookingObject
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $pricePerDay
     * @return BookingObject
     */
    public function setPricePerDay($pricePerDay)
    {
        $this->pricePerDay = $pricePerDay;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPricePerDay()
    {
        return $this->pricePerDay;
    }

    /**
     * @param mixed $latCoord
     * @return BookingObject
     */
    public function setLatCoord($latCoord)
    {
        $this->latCoord = $latCoord;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLatCoord()
    {
        return $this->latCoord;
    }

    /**
     * @param mixed $longCoord
     * @return BookingObject
     */
    public function setLongCoord($longCoord)
    {
        $this->longCoord = $longCoord;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLongCoord()
    {
        return $this->longCoord;
    }

    /**
     * @param Landlord $author
     * @return BookingObject
     */
    public function setAuthor(Landlord $author): BookingObject
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return Landlord
     */
    public function getAuthor(): Landlord
    {
        return $this->author;
    }

    /**
     * @param mixed $type
     * @return BookingObject
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getToArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'numberRooms' => $this->getNumberRooms(),
            'contactPerson' => $this->getContactPerson(),
            'phone' => $this->getPhone(),
            'address' => $this->getAddress(),
            'pricePerDay' => $this->getPricePerDay(),
            'latCoord' => $this->getLatCoord(),
            'longCoord' => $this->getLongCoord(),
            'authorEmail' => $this->getAuthor()->getEmail(),
            'type' => $this->getType()
        ];
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [];
    }

    /**
     * @param mixed $reservations
     * @return BookingObject
     */
    public function setReservations($reservations)
    {
        $this->reservations = $reservations;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReservations()
    {
        return $this->reservations;
    }

}
