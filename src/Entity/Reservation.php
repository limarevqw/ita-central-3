<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ReservationRepository")
 */
class Reservation
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $room;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    private $releaseDate;

    /**
     * @var BookingObject
     * @ORM\ManyToOne(targetEntity="BookingObject", inversedBy="reservations")
     * @ORM\JoinColumn(name="booking_object_id", referencedColumnName="id")
     */
    private $bookingObject;

    /**
     * @var Client
     * @ORM\ManyToOne(targetEntity="App\Entity\Tenant", inversedBy="reservations")
     * @ORM\JoinColumn(name="tenant_id", referencedColumnName="id")
     */
    private $tenant;

    /**
     * @param int $id
     * @return Reservation
     */
    public function setId(int $id): Reservation
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param mixed $room
     * @return Reservation
     */
    public function setRoom($room)
    {
        $this->room = $room;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * @param \DateTime $releaseDate
     * @return Reservation
     */
    public function setReleaseDate(\DateTime $releaseDate): Reservation
    {
        $this->releaseDate = $releaseDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getReleaseDate(): \DateTime
    {
        return $this->releaseDate;
    }

    /**
     * @param BookingObject $bookingObject
     * @return Reservation
     */
    public function setBookingObject(BookingObject $bookingObject): Reservation
    {
        $this->bookingObject = $bookingObject;
        return $this;
    }

    /**
     * @return BookingObject
     */
    public function getBookingObject(): BookingObject
    {
        return $this->bookingObject;
    }

    /**
     * @param Client $tenant
     * @return Reservation
     */
    public function setTenant(Client $tenant): Reservation
    {
        $this->tenant = $tenant;
        return $this;
    }

    /**
     * @return Client
     */
    public function getTenant(): Client
    {
        return $this->tenant;
    }


}