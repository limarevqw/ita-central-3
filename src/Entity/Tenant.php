<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TenantRepository")
 */
class Tenant extends Client
{
    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Reservation", mappedBy="tenant")
     */
    private $reservations;

    /**
     * @param mixed $reservations
     * @return Tenant
     */
    public function setReservations($reservations)
    {
        $this->reservations = $reservations;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getReservations()
    {
        return $this->reservations;
    }
}