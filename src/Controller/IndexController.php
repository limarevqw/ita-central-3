<?php

namespace App\Controller;

use App\Entity\BookingObject;
use App\Entity\Reservation;
use App\Model\BookingObject\BookingObjectHandler;
use App\Model\Client\ClientHandler;
use App\Repository\BookingObjectRepository;
use App\Repository\ClientRepository;
use App\Repository\CottageRepository;
use App\Repository\PensionRepository;
use App\Repository\ReservationRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/ping", name="app_index")
     * @return JsonResponse
     */
    public function pingAction()
    {
        return new JsonResponse(['result' => 'pong']);
    }

    /**
     * @Route("/client-encode-password/{password}", name="/client_encode_password")
     * @param ClientHandler $clientHandler
     * @param string $password
     * @return JsonResponse
     */
    public function encodePasswordAction(ClientHandler $clientHandler ,string $password)
    {
        return new JsonResponse([
            'result' => $clientHandler->encodePassword($password)
        ]);
    }

    /**
     * @Route("/client/{passport}/{email}", name="app_client_exists")
     * @Method("HEAD")
     * @param string $passport
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function clientExistsAction(
        string $passport,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPassportAndEmail($passport, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/auth-client-exists/{password}/{email}", name="auth_client_exists")
     * @Method("HEAD")
     * @param string $password
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function authClientExistsAction(
        string $password,
        string $email,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByPasswordAndEmail($password, $email)) {
            return new JsonResponse(['result' => 'ok']);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/get-one-data-client/{email}", name="app_get_one_data_client")
     * @Method("GET")
     * @param string $email
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     */
    public function getOneDataClientAction(
        string $email,
        ClientRepository $clientRepository)
    {
        $result = $clientRepository->findOneByEmail($email);

        if ($result) {
            $data = $result->toArray();
            $status = true;
        }
        return new JsonResponse([
            'data' => isset($data) ? $data : null ,
            'status' => isset($status) ?? false
        ]);
    }

    /**
     * @Route("/client", name="app_create_client")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createClientAction(
        ClientRepository $clientRepository,
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->request->get('email');
        $data['passport'] = $request->request->get('passport');
        $data['password'] = $request->request->get('password');
        $data['roles'] = $request->request->get('roles');

        if(empty($data['email']) || empty($data['passport']) || empty($data['password'])) {
            return new JsonResponse(['error' => 'Недостаточно данных. Вы передали: '.var_export($data,1)],406);
        }


        if ($clientRepository->findOneByPassportAndEmail($data['passport'], $data['email'])) {
            return new JsonResponse(['error' => 'Клиент уже существует'],406);
        }

        $client = $clientHandler->changeTypeClient($data['roles'][1], $data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/ulogin-client-exists/{uid}/{network}", name="ulogin_client_exists")
     *
     * @Method("HEAD")
     * @param string $uid
     * @param string $network
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     * @internal param string $password
     */
    public function uloginClientExistsClientAction(
        string $uid,
        string $network,
        ClientRepository $clientRepository)
    {
        if ($clientRepository->findOneByUid($uid, $network)) {
            return new JsonResponse(['status' => true]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/ulogin-data-client/{uid}/{network}", name="ulogin_data_client")
     *
     * @Method("GET")
     * @param string $uid
     * @param string $network
     * @param ClientRepository $clientRepository
     * @return JsonResponse
     * @internal param string $password
     */
    public function uloginDataClientAction(
        string $uid,
        string $network,
        ClientRepository $clientRepository)
    {
        $result = $clientRepository->findOneByUid($uid, $network);

        if ($result) {
            return new JsonResponse(['result' => $result->toArray()]);
        } else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * @Route("/create-client-soc", name="app_create_soc_client")
     * @Method("POST")
     * @param ClientHandler $clientHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createSocClientAction(
        ClientHandler $clientHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        $data['email'] = $request->get('email');
        $data['uid'] = $request->get('uid');
        $data['passport'] = $request->get('passport');
        $data['password'] = $request->get('password');
        $data['roles'] = $request->get('roles');
        $network = $request->get('network');

        $client = $clientHandler->changeTypeClient($data['roles'][1], $data, $network);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/update-client-soc", name="app_update_soc_client")
     * @Method("PUT")
     * @param ObjectManager $manager
     * @param Request $request
     * @param ClientRepository $clientRepository
     * @param ClientHandler $clientHandler
     * @return JsonResponse
     */
    public function updateSocClientAction(
        ObjectManager $manager,
        Request $request,
        ClientRepository $clientRepository,
        ClientHandler $clientHandler
    )
    {
        $data['email'] = $request->get('email');
        $data['passport'] = $request->get('passport');
        $data['faceBookId'] = $request->get('faceBookId');
        $data['googleId'] = $request->get('googleId');
        $data['vkId'] = $request->get('vkId');
        $data['password'] = $request->get('password');
        $data['roles'] = $request->get('roles');

        $client = $clientHandler->updateClient($clientRepository, $data);

        $manager->persist($client);
        $manager->flush();

        return new JsonResponse(['result' => 'ok']);
    }

    /**
     * @Route("/create-booking-object", name="app_create_booking_object")
     * @Method("POST")
     * @param ClientRepository $clientRepository
     * @param BookingObjectHandler $bookingObjectHandler
     * @param ObjectManager $manager
     * @param Request $request
     * @return JsonResponse
     */
    public function createBookingObjectAction(
        ClientRepository $clientRepository,
        BookingObjectHandler $bookingObjectHandler,
        ObjectManager $manager,
        Request $request
    )
    {
        try {
            $bookingObjectData = $request->get("BookingObjectData");
            $dopField = $request->get("dopField");
            $email = $request->get("email");

            $data = array_merge($bookingObjectData, $dopField);
            $data['author'] = $clientRepository->findOneByEmail($email);

            switch ($bookingObjectData['type']) {
                case 'pension':
                    $bookingObject = $bookingObjectHandler->createNewPension($data);
                    break;
                case 'сottage':
                    $bookingObject = $bookingObjectHandler->createNewCottage($data);
                    break;
                default:
                    return new JsonResponse(['error' => 'Неверный тип обьекта.'],406);
                    break;
            }

            $manager->persist($bookingObject);
            $manager->flush();

            return new JsonResponse(['result' => 'ok']);

        } catch (\Exception $e) {

            return new JsonResponse([
                'result' => 'error',
                'message' => $e->getMessage()
            ]);
        }
    }

    /**
     * @Route("/all-booking-object", name="app_all_booking_object")
     * @Method("GET")
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     */
    public function allBookingObjectAction(BookingObjectRepository $bookingObjectRepository)
    {
        $result = [];
        $message = "ok";
        try {
            /** @var BookingObject[] $bookingobjects */
            $bookingobjects = $bookingObjectRepository->findByAll();

            foreach ($bookingobjects as $bookingObject) {
                $result[] = $bookingObject->toArray();
            }
        } catch (\Exception $e) {

            $message = $e->getMessage();
        }

        return new JsonResponse([
            'result' => $result,
            'message' => $message
        ]);

    }

    /**
     * @Route("/one-booking-object/{id}", name="app_one_booking_object")
     * @Method("GET")
     * @param Request $request
     * @param ReservationRepository $reservationRepository
     * @param BookingObjectRepository $bookingObjectRepository
     * @return JsonResponse
     */
    public function oneBookingObjectAction(
        Request $request,
        ReservationRepository $reservationRepository,
        BookingObjectRepository $bookingObjectRepository)
    {
        $result = [];
        $message = "ok";
        try {
            $id = $request->get("id");

            $bookingObject = $bookingObjectRepository->find($id);

            $bookingObjectArray = $bookingObject->toArray();

            $numbers = [];
            for ($number = 1; $number <= $bookingObject->getNumberRooms(); $number++) {

                /** @var Reservation $reservation */
                $reservation = $reservationRepository->findLastReleaseDate(
                    $bookingObject,
                    $number
                );

                if ($reservation) {
                    $numbers[] = [
                        'numberLastDate' => $reservation->getReleaseDate()->format("d/m/Y")
                    ];
                } else {
                    $numbers[] = [
                        'numberLastDate' => 'нет броней'
                    ];
                }
            }

            $bookingObjectArray['numbers'] = $numbers;

            $result[] = $bookingObjectArray;
        } catch (\Exception $e) {

            $message = $e->getMessage();
        }

        return new JsonResponse([
            'result' => $result,
            'message' => $message
        ]);

    }

    /**
     * @Route("/search-booking-objects", name="app_search_booking_objects")
     * @Method("POST")
     * @param Request $request
     * @param PensionRepository $pensionRepository
     * @param CottageRepository $cottageRepository
     * @return JsonResponse
     */
    public function searchBookingObjectsAction(
        Request $request,
        PensionRepository $pensionRepository,
        CottageRepository $cottageRepository
)
    {

        $result = [];
        $message = "ok";
        try {
            $name = $request->get("name");
            $priceFrom = intval($request->get("priceFrom"));
            $priceTo = intval($request->get("priceTo"));
            $typeObject = $request->get("typeObject");
            $dopField = $request->get("dopField");

            $data = [
                'name' => $name,
                'priceFrom' => $priceFrom,
                'priceTo' => $priceTo,
                'typeObject' => $typeObject,
                'dopField' => $dopField,
            ];

            switch ($typeObject) {
                case "сottage":
                    $bookingobjects = $cottageRepository->findBySearch($data);
                    break;
                default:
                    $bookingobjects = $pensionRepository->findBySearch($data);
                    break;
            }

            /** @var BookingObject $bookingObject */
            foreach ($bookingobjects as $bookingObject) {
                $result[] = $bookingObject->toArray();
            }

        } catch (\Exception $e) {
            $message = $e->getMessage();
        }

        return new JsonResponse([
            'result' => $result,
            'message' => $message
        ]);

    }
}
