<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\Client;

use App\Entity\Client;
use App\Entity\Landlord;
use App\Entity\Tenant;
use App\Repository\ClientRepository;

class ClientHandler
{

    /**
     * @param Client $client
     * @param array $data
     * @param bool $network
     * @return Client
     */
    public function createNewAbstractClient(Client $client, array $data, $network = false)
    {
        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);

        if ($network) {
            $client->setNetworkUlogin($network, $data['uid']);
        }

        $password = $this->encodePassword($data['password']);
        $client->setPassword($password);
        $client->setRoles($data['roles'] ?? ['ROLE_USER']);

        return $client;
    }

    /**
     * @param ClientRepository $clientRepository
     * @param array $data
     * @return Client
     */
    public function updateClient(ClientRepository $clientRepository, $data)
    {
        $client = $clientRepository->findOneByEmail($data['email']);

        $client->setEmail($data['email']);
        $client->setPassport($data['passport']);
        $client->setFaceBookId($data['faceBookId'] ?? null);
        $client->setGoogleId($data['googleId'] ?? null);
        $client->setVkId($data['vkId'] ?? null);
        $client->setPassword($data['password']);
        $client->setRoles($data['roles']);

        return $client;
    }

    /**
     * @param $password
     * @return string
     */
    public function encodePassword($password)
    {
        return md5($password).md5($password.'2');
    }

    /**
     * @param array $data
     * @param bool $network
     * @return Client
     */
    public function createNewClient(array $data, $network = false)
    {
        /** @var Client $client */
        $client = $this->createNewAbstractClient(new Client(), $data, $network);
        return $client;
    }

    /**
     * @param array $data
     * @param bool $network
     * @return Tenant
     */
    public function createNewTenant(array $data, $network = false)
    {

        /** @var Tenant $client */
        $client = $this->createNewAbstractClient(new Tenant(), $data, $network);
        return $client;
    }

    /**
     * @param array $data
     * @param bool $network
     * @return Landlord
     */
    public function createNewLandlord(array $data, $network = false)
    {
        /** @var Landlord $client */
        $client = $this->createNewAbstractClient(new Landlord(), $data, $network);
        return $client;
    }

    public function changeTypeClient($roles, array $data, $network = false)
    {
        switch ($roles) {
            case 'ROLE_TENANT':
                $client = $this->createNewTenant($data, $network);
                break;
            case 'ROLE_LANDLORD':
                $client = $this->createNewLandlord($data, $network);
                break;
            default:
                $client = $this->createNewTenant($data, $network);
                break;
        }

        return $client;
    }
}
