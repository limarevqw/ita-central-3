<?php

namespace App\Model\BookingObject;


use App\Entity\BookingObject;
use App\Entity\Cottage;
use App\Entity\Pension;

class BookingObjectHandler
{
    /**
     * @param BookingObject $bookingObject
     * @param array $data
     * @return BookingObject
     */
    public function createNewAbstractBookingObject(BookingObject $bookingObject, array $data)
    {
        $bookingObject->setName($data['name'] ?? null);
        $bookingObject->setNumberRooms($data['numberRooms'] ?? null);
        $bookingObject->setContactPerson($data['contactPerson'] ?? null);
        $bookingObject->setPhone($data['phone'] ?? null);
        $bookingObject->setAddress($data['address'] ?? null);
        $bookingObject->setPricePerDay($data['pricePerDay'] ?? 0);
        $bookingObject->setLatCoord($data['lat'] ?? null);
        $bookingObject->setLongCoord($data['long'] ?? null);
        $bookingObject->setAuthor($data['author'] ?? null);
        return $bookingObject;
    }

    /**
     * @param array $data
     * @return Pension
     */
    public function createNewPension(array $data)
    {
        /** @var Pension $pension */
        $pension = $this->createNewAbstractBookingObject(new Pension(), $data);
        $pension->setBigTV($data['BigTV'] ? true : false);
        $pension->setBunkBeds($data['bunkBeds'] ? true : false);
        $pension->setShower($data['shower'] ? true : false);
        $pension->setType("pension");
        return $pension;
    }

    /**
     * @param array $data
     * @return Cottage
     */
    public function createNewCottage(array $data)
    {
        /** @var Cottage $cottage */
        $cottage = $this->createNewAbstractBookingObject(new Cottage(), $data);
        $cottage->setKitchen($data['Kitchen'] ? true : false);
        $cottage->setBathroom($data['bathroom'] ? true : false);
        $cottage->setJacuzzi($data['jacuzzi'] ? true : false);
        $cottage->setPool($data['pool'] ? true : false);
        $cottage->setGarage($data['garage'] ? true : false);
        $cottage->setType("cottage");
        return $cottage;
    }
}